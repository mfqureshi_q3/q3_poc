from flask import Flask, render_template, redirect, url_for, flash, get_flashed_messages, request, session
from urllib.parse import unquote
from json.encoder import JSONEncoder
from json.decoder import JSONDecoder
from flask_wtf import FlaskForm
from base64 import b64encode
from flask_wtf.file import FileAllowed, FileStorage
from wtforms import validators, StringField, PasswordField, FileField
from flask_wtf.csrf import CSRFProtect
from flask_mongoengine import MongoEngine
import mongoengine.fields as field
from mongoengine.errors import NotUniqueError

SECRET_KEY = "secret key"
app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['MONGODB_SETTINGS'] = {
    'db': 'q3_poc',
    'host': '127.0.0.1',
    'port': 27017
}
csrf = CSRFProtect(app)
db = MongoEngine(app)

class User(db.Document):
    name = field.StringField()
    email = field.EmailField(unique=True)
    password = field.StringField(min_length=8)
    dp = field.StringField()

class Signup(FlaskForm):
    name = StringField('Name', validators=[validators.required()], id="name")
    email = StringField('Email', validators=[validators.required(), validators.email()], id="email")
    password = PasswordField('Password', validators=[validators.required(), validators.length(min=8)], id="password")
    dp = FileField('Choose Profile Picture', validators=[validators.optional(), FileAllowed(('jpg', 'jpeg', 'png', 'gif', 'bmp'), 'Select Valid Image!')], id="dp")
    #validators.regexp('([^\\s]+(\\.(?i)(jpe?g|png|gif|bmp))$)')

class Signin(FlaskForm):
    email = StringField('Email', validators=[validators.required(), validators.email()],)
    password = PasswordField('Password', validators=[validators.required(), validators.length(min=8)])

@app.route('/', methods=["GET", "POST"])
def index():
    if not session.get('uid'):
        return redirect(url_for('signin'))#render_template('index.html')
    return render_template('index.html')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = Signup()
    #print(form.errors)
    #print("ALL USERS------------->UP>")
    #print(*[(u.name, u.email, u.password, u.dp) for u in User.objects], sep='\n')
    if form.validate_on_submit():
        #print(form.dp.raw_data[0].__dict__, form.dp.__dict__, User.objects(email__exact=form.email.data))
        user = User(email=form.email.data)
        user.name = form.name.data
        user.password = form.password.data
        print(f"Raw:{form.dp.raw_data[0]}")
        user.dp = get_image_as_str(form.dp.raw_data[0])
        print(f"Str:{user.dp}")
        try:
            user.save()
        except NotUniqueError:
            #print(User.objects)
            flash("Email Already In Use! Please use different email.")
            return redirect(url_for('signup'))
        session['uid'] = user.email
        return redirect(url_for('index'))
    return render_template('signup.html', form=form)


@app.route('/signin/', methods=['GET', 'POST'])
def signin():
    # Highly Insecure Password Handling #
    form = Signin()
    if form.validate_on_submit():
        user = User.objects(email__exact=form.email.data)
        #print("ALL USERS------------->IN>")
        #print(*[(u.name, u.email, u.password) for u in User.objects], sep='\n')
        if user:
            user = user[0]
            if user.password == form.password.data:
                session['uid'] = user.email
                return redirect(url_for('index'))
            else:
                return "Incorrect password"
        else:
            flash("Account Not Found! Please Sign Up.")
            return redirect(url_for('signup'))
    return render_template('signin.html', form=form)

@app.route("/signout/")
def signout():
    session['uid'] = None
    return redirect(url_for('index'))

@app.route("/images", methods=["POST"])
def images():
    try:
        print("IMAGE LODAER")
        print(request.args.get("result"))
        if request.method == "POST":
            print("IMAGE PUT")
        return JSONEncoder().encode({'value':'IMAGELOADER_JSON_RESPONSE'})
    except KeyError:
        print("AAAAAAAAAAAAAAAAAAAAAAAA")
        return "Failed"

@app.route("/search/")
@app.route("/search/<string:text>")
def search(text):
    if not session['uid']: return ''

    if text:
        data = JSONDecoder().decode(unquote(text))
        if session['uid'] == data["email"]:
            return "<div class='text-center'><h1>YES YOU EXIST!!</h1></div>"
        user = User.objects(email=data["email"])
        if len(user) == 0:
            return "<div class='text-center'><h1>No User Found!</h1></div>"
        else:
            return render_template("search.html", user=user[0], form=Signup())


@app.route("/profile/")
@app.route("/profile/<string:text>", methods=["GET", "POST"])
def profile(text=''):
    if not session['uid']:
        return ""
    user = User.objects(email__exact=session['uid'])[0]
    if text:
        data = JSONDecoder().decode(unquote(text))
        user.update(
            name=data['name'],
            email=data['email'],
            password=data['password'],
#            dp=get_image_as_str(form.dp.raw_data[0])
        )
        user.reload()
        session['uid'] = user.email
        print("PROFILE USER RELOAD")
        print(data)
    form = Signup()
    print("PROFILE")
    return render_template("profile.html", user=user, form=form)#f"Username:{user.name}<br>Email:{user.email}<br><img src={user.dp}>"

def get_image_as_str(raw: FileStorage) -> str:
    '''
    Represents raw image data as Data URI that can be used as following in HTML to display image <img src=get_image_as_str(raw)>
    :param raw: raw_data read from WTForms
    :return: ascii string in th following format "data:<content_type>;base64,<image_data>"
    '''
    stream = raw.stream.read()
    print(f"Stream:{stream}")
    if not stream:
        return url_for('static', filename='images/avatar.png')
    return f"data:{raw.content_type};base64,{b64encode(stream).decode('ascii')}"

if __name__ == '__main__':
    app.run()
